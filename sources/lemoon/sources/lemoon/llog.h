/**
 * @file     llog
 * @brief    Copyright (C) 2015  yayanyang All Rights Reserved 
 * @author   yayanyang
 * @version  1.0.0.0  
 * @date     2015/02/02
 */
#ifndef LEMOON_LLOG_H
#define LEMOON_LLOG_H
#include <lemoon/lemoon.h>


LEMOON_PRIVATE EXTERN_C int llog_print(lua_State *L);

#endif  //LEMOON_LLOG_H